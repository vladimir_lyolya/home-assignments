package ru.sberbank.jd.lesson08.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Comparator;

public class CustomTreeMapImplTest {

    Comparator<Integer> compare = Integer::compare;

    CustomTreeMapImpl<Integer, String> myTree = new CustomTreeMapImpl<>(compare);

    @Before
    public void setTreeMap() {
        myTree.put(10, "101010");
        myTree.put(11, "111111");
        myTree.put(9, "090909");
        myTree.put(7, "070707");
        myTree.put(6, "060606");
        myTree.put(13, "131313");
    }

    @Test
    public void size() {
        CustomTreeMapImpl<Integer, String> myTreeForSize = new CustomTreeMapImpl<>(compare);
        Assert.assertEquals(0, myTreeForSize.size());
        myTreeForSize.put(5, "5555");
        Assert.assertEquals(1, myTreeForSize.size());
    }

    @Test
    public void get() {
        Assert.assertEquals("060606", myTree.get(6));
    }

    @Test
    public void containsKey() {
        Assert.assertTrue(myTree.containsKey(6));
        Assert.assertFalse(myTree.containsKey(100));
    }

    @Test
    public void containsValue() {
        Assert.assertTrue(myTree.containsValue("060606"));
        Assert.assertFalse(myTree.containsValue("asdf"));
    }

    @Test
    public void removeRootOne() {
        CustomTreeMapImpl<Integer, String> myTreeForSize = new CustomTreeMapImpl<>(compare);
        myTreeForSize.put(5, "5555");
        Assert.assertEquals(1, myTreeForSize.size());
        Assert.assertEquals("5555", myTreeForSize.remove(5));
        Assert.assertEquals(0, myTreeForSize.size());
    }

    @Test
    public void removeRoot() {
        int size = myTree.size();
        Assert.assertEquals("101010", myTree.remove(10));
        Assert.assertFalse(myTree.containsKey(10));
        Assert.assertEquals(size - 1, myTree.size());
    }

    @Test
    public void removeLast() {
        int size = myTree.size();
        Assert.assertEquals("060606", myTree.remove(6));
        Assert.assertFalse(myTree.containsKey(6));
        Assert.assertEquals(size - 1, myTree.size());
    }

    @Test
    public void removeMiddle() {
        int size = myTree.size();
        Assert.assertEquals("090909", myTree.remove(9));
        Assert.assertFalse(myTree.containsKey(9));
        Assert.assertEquals(size - 1, myTree.size());
    }

    @Test
    public void put() {
        Assert.assertNull(myTree.put(200, "bbb"));
        Assert.assertEquals("101010", myTree.put(10, "aaa"));
        Assert.assertEquals("aaa", myTree.get(10));
        Assert.assertTrue(myTree.containsKey(200));
        Assert.assertEquals("bbb", myTree.get(200));
    }

    @Test
    public void isEmpty() {
        CustomTreeMapImpl<Integer, String> myTreeForSize = new CustomTreeMapImpl<>(compare);
        Assert.assertTrue(myTreeForSize.isEmpty());
        myTreeForSize.put(5, "5555");
        Assert.assertFalse(myTreeForSize.isEmpty());
    }

    @Test
    public void getKey() {
        Object[] tmpKey = myTree.keys();
        Integer[] toCompare = {6, 7, 9, 13, 11, 10};
        for (int i = 0; i < tmpKey.length; i++) {
            Assert.assertEquals(toCompare[i], tmpKey[i]);
        }
    }

    @Test
    public void getValue() {
        Object[] tmpValue = myTree.values();
        String[] toCompare = {"060606", "070707", "090909", "131313", "111111", "101010"};
        for (int i = 0; i < tmpValue.length; i++) {
            Assert.assertEquals(toCompare[i], tmpValue[i]);
        }
    }
}