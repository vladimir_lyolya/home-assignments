package ru.sberbank.jd.lesson08.impl;

import ru.sberbank.jd.lesson08.CustomTreeMap;

import java.lang.reflect.Array;
import java.util.Comparator;

public class CustomTreeMapImpl<K, V> implements CustomTreeMap<K, V> {

    private class Node {
        private K key;
        private V val;
        private Node left, right;
        private Node parent;
        private int length;

        public Node(K key, V val, int length, Node parent) {
            this.key = key;
            this.val = val;
            this.length = length;
            this.parent = parent;
        }
    }

    Comparator<K> extCompare;

    private Node root;
    private V oldValuePut;
    private int index;

    public CustomTreeMapImpl(Comparator<K> extCompare) {
        this.extCompare = extCompare;
    }

    private boolean containsKey(Node node, K key) {
        if (node == null) return false;
        int cmp = extCompare.compare(key, node.key);
        if (cmp < 0) {
            return containsKey(node.left, key);
        } else if (cmp > 0) {
            return containsKey(node.right, key);
        } else {
            return true;
        }
    }

    private boolean containsValue(Node node, V value) {
        boolean resultLeft = false;
        boolean resultRight = false;
        if (node.val == value) {
            return true;
        } else {
            if (node.left != null) {
                resultLeft = containsValue(node.left, value);
            }
            if (resultLeft) return resultLeft;
            if (node.right != null) {
                resultRight = containsValue(node.right, value);
            }
            return resultRight;
        }
    }


    private Node search(Node node, K key) {
        if (node == null) return null;
        int cmp = extCompare.compare(key, node.key);
        if (cmp < 0) {
            return search(node.left, key);
        } else if (cmp > 0) {
            return search(node.right, key);
        } else {
            return node;
        }
    }

    private void updateSize(Node node) {
        if (node.left != null) {
            updateSize(node.left);
        }
        if (node.right != null) {
            updateSize(node.right);
        }
        node.length = size(node.left) + size(node.right) + 1;
    }

    private void fillArrayNode(Node node, Node[] arrayNode) {
        if (node.left != null) {
            fillArrayNode(node.left, arrayNode);
        }
        if (node.right != null) {
            fillArrayNode(node.right, arrayNode);
        }
        arrayNode[index] = node;
        index++;
    }

    private void fillArrayKey(Node node, K[] arrayKey) {
        if (node.left != null) {
            fillArrayKey(node.left, arrayKey);
        }
        if (node.right != null) {
            fillArrayKey(node.right, arrayKey);
        }
        arrayKey[index] = node.key;
        index++;
    }

    private void fillArrayValue(Node node, V[] arrayValue) {
        if (node.left != null) {
            fillArrayValue(node.left, arrayValue);
        }
        if (node.right != null) {
            fillArrayValue(node.right, arrayValue);
        }
        arrayValue[index] = node.val;
        index++;
    }

    /**
     * Так как компаратор внешний и правило удаления не задано, после удаления вставляем висящие ветки
     * на общих основаниях вставки в дерево
     */
    private V remove(Node node, K key) {
        Node tmpNode = search(node, key);
        if (tmpNode == null) {
            return null;
        }
        Node[] tmpArrayNode = (Node[]) Array.newInstance(Node.class, size(tmpNode));
        index = 0;
        V oldValueRemove = tmpNode.val;
        fillArrayNode(tmpNode, tmpArrayNode);
        //Удаляем найденную ноду
        if (tmpNode.parent == null) root = null;
        else if (tmpNode.parent.left == tmpNode) tmpNode.parent.left = null;
        else if (tmpNode.parent.right == tmpNode) tmpNode.parent.right = null;
        if (tmpArrayNode.length == 1) {         //Если удалили последний элемент в ветке то необходим просто апдейт size
            if (root != null) updateSize(root); //Если удалии root то ничего не делаем
        } else
            for (int i = 0; i < tmpArrayNode.length - 1; i++) {
                root = put(root, tmpArrayNode[i].key, tmpArrayNode[i].val, root);
            }
        return oldValueRemove;
    }

    private int size(Node node) {
        return node == null ? 0 : node.length;
    }

    private Node put(Node node, K key, V val, Node parent) {
        if (node == null) {
            oldValuePut = null;
            return new Node(key, val, 1, parent);
        }
        int cmp = extCompare.compare(key, node.key);
        if (cmp < 0) {
            node.left = put(node.left, key, val, node);
        } else if (cmp > 0) {
            node.right = put(node.right, key, val, node);
        } else {
            oldValuePut = node.val;
            node.val = val;
        }
        node.length = size(node.left) + size(node.right) + 1;
        return node;
    }

    public int size() {
        return size(root);
    }

    public V get(K key) {
        Node node = search(root, key);
        return node == null ? null : node.val;
    }

    public V put(K key, V val) {
        root = put(root, key, val, root);
        return oldValuePut;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public V remove(K key) {
        return remove(root, key);
    }

    public boolean containsKey(K key) {
        return containsKey(root, key);
    }

    public boolean containsValue(V value) {
        return containsValue(root, value);
    }

    public Object[] keys() {
        index = 0;
        K[] arrayKey = (K[]) new Object[size(root)];
        fillArrayKey(root, arrayKey);
        return arrayKey;
    }

    public Object[] values() {
        index = 0;
        V[] arrayValue = (V[]) new Object[size(root)];
        fillArrayValue(root, arrayValue);
        return arrayValue;
    }
}
