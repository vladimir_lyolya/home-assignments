package ru.sberbank.jd.lesson01;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Неизменяемый класс, возвращает персональную информацию
 */
public class VladimirLyolyaGreetingImpl implements Greeting {

    public  final String courseExpectations1 = "Овладеть языком Java";
    public  final String courseExpectations2 = "Овладеть современными инструментами разработки";
    public  final String courseExpectations3 = "Получить опыт и создать клиент-серверное приложение с интерфейсом пользователя";
    public  final String educationInfoTest1 = "Уч. заведение:\t Батаский техникум ж/д транспорта, \n" +
                                               "Факультет:\t\t «Автоматика и телемеханика», \n" +
                                               "Специальность:\t «ТО и ремонт автом. систем управления движением на ж/д тр-те.»";
    public  final String educationInfoTest2 = "Уч. заведение:\t Ростовская-на-Дону государственная академия сельскохозяйственного машиностроения, \n" +
                                               "Факультет:\t\t «Автоматика и робототехника», \n" +
                                               "Специальность:\t «Электропривод и автоматика промышленных установок " +
                                               "и технологических комплексов»";
    private final String firstName;
    private final String secondName;
    private final String lastName;
    private final String birthYear;
    private final String bitbucketUrl;
    private final String phone;
    private final Collection<String> hobbies;
    private final Collection<String> courseExpectations;
    private final Collection<String> educationInfo;

    public VladimirLyolyaGreetingImpl(){

        firstName = "Vladimir";
        secondName = "Sergeyevich";
        lastName = "Lyolya";
        birthYear = "1976";
        bitbucketUrl = "https://bitbucket.org/vladimir_lyolya/home-assignments";
        phone = "+79604641374";
        hobbies = new ArrayList<>();
        Collections.addAll(hobbies, "Чтение","Музыка","Мотоциклы");
        courseExpectations = new ArrayList<>();
        Collections.addAll(courseExpectations, courseExpectations1, courseExpectations2, courseExpectations3);
        educationInfo = new ArrayList<>();
        Collections.addAll(educationInfo, educationInfoTest1, educationInfoTest2);
    }
    /**
     * Get first name.
     */
    public String getFirstName(){
        return firstName;
    }

    /**
     * Get second name
     */
    public String getSecondName(){
        return secondName;
    }

    /**
     * Get last name.
     */
    public String getLastName(){
        return lastName;
    }

    /**
     * Get birth year.
     */
    public String getBirthYear(){
        return birthYear;
    }

    /**
     * Get hobbies.
     */
    public Collection<String> getHobbies(){
        return hobbies;
    }

    /**
     * Get bitbucket url to your repo.
     */
    public String getBitbucketUrl(){
        return bitbucketUrl;
    }

    /**
     * Get phone number.
     */
    public String getPhone(){
        return phone;
    }

    /**
     * Your expectations about course.
     */
    public Collection<String> getCourseExpectations() {
        return courseExpectations;
    }

    /**
     * Print your university and faculty here.
     */
    public Collection<String> getEducationInfo(){
        return educationInfo;
    }
}
