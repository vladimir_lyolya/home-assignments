package ru.sberbank.jd.lesson01;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

    /**
     * Проверка
     */
public class VladimirLyolyaGreetingImplTest {

    private final VladimirLyolyaGreetingImpl objForTest = new VladimirLyolyaGreetingImpl();

    /**
     * Проверка Get first name.
     */
    @Test
    public void getFirstNameTest(){
        Assert.assertEquals("getFirstNameTest fail ", "Vladimir", objForTest.getFirstName());
    }

    /**
     * Проверка Get Second Name.
     */
    @Test
    public void getSecondNameTest(){
        Assert.assertEquals("getSecondNameTest fail ", "Sergeyevich", objForTest.getSecondName());
    }

    /**
     * Проверка Get Last Name.
     */
    @Test
    public void getLastNameTest(){
        Assert.assertEquals("getLastNameTest fail ", "Lyolya", objForTest.getLastName());
    }

    /**
     * Проверка Get Birth Year.
     */
    @Test
    public void getBirthYearTest(){
        Assert.assertEquals("getBirthYearTest fail ", "1976", objForTest.getBirthYear());
    }

    /**
     * Проверка Get Bitbucket tUrl.
     */
    @Test
    public void getBitbucketUrlTest(){
        Assert.assertEquals("getBitbucketUrlTest fail ", "https://bitbucket.org/vladimir_lyolya/home-assignments",
                objForTest.getBitbucketUrl());
    }

    /**
     * Проверка Get Phone.
     */
    @Test
    public void getPhoneTest(){
        Assert.assertEquals("getPhoneTest fail ", "+79604641374", objForTest.getPhone());
    }

    /**
     * Проверка Get Hobbies.
     */
    @Test
    public void getHobbiesTest(){
        List<String> tstHobbies = (List<String>)objForTest.getHobbies();
        Assert.assertEquals("getHobbiesTest fail ", "Чтение", tstHobbies.get(0));
        Assert.assertEquals("getHobbiesTest fail ", "Музыка", tstHobbies.get(1));
        Assert.assertEquals("getHobbiesTest fail ", "Мотоциклы", tstHobbies.get(2));

    }

    /**
     * Проверка Get Course Expectations.
     */
    @Test
    public void getCourseExpectationsTest(){
        List<String> tsCourseExp = (List<String>)objForTest.getCourseExpectations();
        Assert.assertEquals("getCourseExpectationsTest fail ", objForTest.courseExpectations1, tsCourseExp.get(0));
        Assert.assertEquals("getCourseExpectationsTest fail ", objForTest.courseExpectations2, tsCourseExp.get(1));
        Assert.assertEquals("getCourseExpectationsTest fail ", objForTest.courseExpectations3, tsCourseExp.get(2));
    }

    /**
     * Проверка Get Education Info.
     */
    @Test
    public void getEducationInfoTest(){
        List<String> tsCourseExp = (List<String>)objForTest.getEducationInfo();
        Assert.assertEquals("getEducationInfoTest fail ", objForTest.educationInfoTest1, tsCourseExp.get(0));
        Assert.assertEquals("getEducationInfoTest fail ", objForTest.educationInfoTest2, tsCourseExp.get(1));
    }
}
