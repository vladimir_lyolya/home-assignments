package ru.sberbank.jd.lesson02;

import org.junit.Assert;
import org.junit.Test;

public class NodImplTest {

    private final NodImpl objForTest = new NodImpl();

    @Test
    public void calculateTest(){
        /**
         * Тестовая последовательномть первого числа
         * {1, 0, 1, 0, 132, 11596, 101, 46,   Integer.MAX_VALUE -3}
         * второго числа
         * {1, 1, 0, 0, 11,  52,    505, 9658, 2563}
         * результат
         * {1, 1, 1, 0, 11,  52,    101, 2,    233}
         */
        int[] first = {1, 0, 1, 0, 132, 11596, 101, 46, Integer.MAX_VALUE -3};
        int[] second = {1, 1, 0, 0, 11, 52, 505, 9658, 2563};
        int[] result = {1, 1, 1, 0, 11, 52, 101, 2, 233};
        for (int i = 0; i < result.length; i++) {
            Assert.assertEquals("calculateTest fail, индекс result = " + i, result[i], objForTest.calculate(first[i], second[i]));
        }

    }

    @Test
    public void calculateTestManualOne(){
        Assert.assertEquals(84, objForTest.calculate(4 * 3 * 5 * 7, 4 * 4 * 3 * 4 * 7));
    }

    @Test
    public void calculateTestManualTwo(){
        Assert.assertEquals(135, objForTest.calculate(3 * 3 * 3 * 4 * 5 * 3, 3 * 3 * 5 * 5 * 3 * 7));
    }
}
