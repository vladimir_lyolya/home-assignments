package ru.sberbank.jd.lesson02;

import java.util.*;

public class NodImpl implements Nod{
    /**
     * Проверяет является число простым или нет
     * @param  num число
     * @return true если простое
     */
    public static boolean isPrime(int num) {
        boolean isTmpPrime = true;
        for (int i = 2; i * i <= num; i++) {
            if ((num % i) == 0) {
                isTmpPrime = false;
                break;
            }
        }
        return isTmpPrime;
    }

    /**
     * Возвращает разложение числа на простые сомножители
     * @param  num число
     * @return ArrayList разложения
     */
    public static ArrayList<Integer> getDecompositionNum(int num){
        ArrayList<Integer> listOfDividers = new ArrayList<>();
        if (num == 0) {
            listOfDividers.add(0);
            return listOfDividers;
        }
        if (num == 1) {
            listOfDividers.add(1);
            return listOfDividers;
        }
        do {
            for (int i = 2; i < Integer.MAX_VALUE; i++) {
                if (isPrime(i)) {
                    if ((num % i) == 0) {
                        listOfDividers.add(i);
                        num = num / i;
                        break;
                    }
                }
            }
        } while (num != 1);
        return listOfDividers;
    }

    /**
     * вычисляет наибольший общий делитель двух целых числел
     * @param first первое число
     * @param second второе число
     * @return наибольший общий делитель
     */
    public int calculate(int first, int second){
        first  = Math.abs(first);
        second = Math.abs(second);

        ArrayList<Integer> listOfDividersFirst = getDecompositionNum(first);
        ArrayList<Integer> listOfDividersSecond = getDecompositionNum(second);
        HashSet<Integer> listOfDividersUnique = new HashSet<>(listOfDividersFirst);

        int nod = 1;
        for(Integer tmpInt: listOfDividersUnique){
             int cnt1 = Collections.frequency(listOfDividersFirst, tmpInt);
             int cnt2 = Collections.frequency(listOfDividersSecond, tmpInt);
             nod *= Math.pow(tmpInt, Math.min(cnt1, cnt2));
        }
        return nod;
    }
}
