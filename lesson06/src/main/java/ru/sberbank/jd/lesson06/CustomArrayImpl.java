package ru.sberbank.jd.lesson06;

import java.util.Arrays;
import java.util.Collection;

public class CustomArrayImpl<T> implements CustomArray<T> {

    private final int DEFAULT_CAP = 2;
    private int size;
    private int capacity;
    private T[] customArray;

    public CustomArrayImpl() {
        customArray = (T[]) new Object[DEFAULT_CAP];
        size = 0;
        capacity = DEFAULT_CAP;
    }

    public CustomArrayImpl(int capacity) {
        if (capacity < 0) throw new IllegalArgumentException("In constructor capacity < || = 0");
        if (capacity == 0) {
            this.capacity = 1;
        } else {
            this.capacity = capacity;
        }
        customArray = (T[]) new Object[this.capacity];
        size = 0;

    }

    public CustomArrayImpl(Collection<T> c) {
        if (c == null) throw new IllegalArgumentException("In constructor c = null");
        customArray = (T[]) c.toArray();
        size = customArray.length;
        capacity = size;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean add(T item) {
        if (size == capacity) {
            addLengthArray(capacity);
        }
        customArray[size] = item;
        size++;
        return true;
    }

    @Override
    public boolean addAll(T[] items) {
        if (items == null) {
            throw new IllegalArgumentException("In method addAll(Object[] items) items is null");
        }
        addArray(items);
        return true;
    }

    @Override
    public boolean addAll(Collection<T> items) {
        if (items == null) throw new IllegalArgumentException("In method addAll(Collection items) items is null");
        T[] tempArray = (T[]) items.toArray();
        /*
            Смотрел https://stackoverflow.com/questions/9572795/convert-list-to-array-in-java
            смотрел Collection.java там "Note that toArray(new Object[0]) is identical in function to toArray()."
            Вроде одно и тоже, или я не то смотрю )
        */
        addArray(tempArray);
        return true;
    }

    @Override
    public boolean addAll(int index, T[] items) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("In function addAll(int index, Object[] items) index < 0 || index > size");
        }
        if (items == null) {
            throw new IllegalArgumentException("In function addAll(int index, Object[] items) items is null");
        }
        if (capacity - size < items.length) {
            addLengthArray(items.length - (capacity - size));
        }
        System.arraycopy(customArray, index, customArray, index + items.length, size - index);
        System.arraycopy(items, 0, customArray, index, items.length);
        size += items.length;
        return true;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("In function get index < 0 || index >= size");
        }
        return customArray[index];
    }

    @Override
    public T set(int index, T item) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("In function set index < 0 || index >= size");
        }
        T oldValue = customArray[index];
        customArray[index] = item;
        return oldValue;
    }

    @Override
    public void remove(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("In function remove index < 0 || index >= size");
        }
        System.arraycopy(customArray, index + 1, customArray, index, size - index - 1);
        size--;
        customArray[size] = null;
    }

    @Override
    public boolean remove(T item) {
        for (int i = 0; i < size; i++) {
            if (customArray[i].equals(item)) {
                remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean contains(T item) {
        for (int i = 0; i < size; i++) {
            if (customArray[i].equals(item)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int indexOf(Object item) {
        for (int i = 0; i < size; i++) {
            if (customArray[i].equals(item)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void ensureCapacity(int newElementsCount) {
        if (capacity - size < newElementsCount) {
            addLengthArray(newElementsCount - (capacity - size));
        }
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public void reverse() {
        for (int i = 0; i < size / 2; i++) {
            T temp = customArray[i];
            customArray[i] = customArray[size - 1 - i];
            customArray[size - 1 - i] = temp;
        }
    }

    @Override
    public Object[] toArray() {
        Object[] copyArray = new Object[size];
        System.arraycopy(customArray, 0, copyArray, 0, size);
        return copyArray;
    }

    @Override
    public String toString() {
        return Arrays.toString(Arrays.copyOf(customArray, size));
    }

    /**
     * Увеличивает емкость внутреннего массива на number элементов
     */
    private void addLengthArray(int number) {
        customArray = Arrays.copyOf(customArray, capacity + number);
        capacity += number;
    }

    /**
     * Добавляет массив к customArray
     */
    private void addArray(T[] array) {
        if (capacity - size < array.length) {
            addLengthArray(array.length - (capacity - size));
        }
        System.arraycopy(array, 0, customArray, size, array.length);
        size += array.length;
    }
}
