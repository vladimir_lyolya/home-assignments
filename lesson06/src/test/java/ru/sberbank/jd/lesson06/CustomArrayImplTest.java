package ru.sberbank.jd.lesson06;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

public class CustomArrayImplTest {

    CustomArrayImpl<String> defaultConstructor;
    CustomArrayImpl<String> intConstructor;
    CustomArrayImpl<String> intConstructorNotNul;
    Collection<Integer> collection;
    CustomArrayImpl<Integer> collectionConstructor;

    public void addToCollection() {

        for (int i = 0; i < 50; i++) {
            collection.add(i);
        }
    }

    public void addToDefault(int number) {
        for (int i = 0; i < number; i++) {
            defaultConstructor.add(String.valueOf(i));
        }
    }

    public void addToInteger(int number) {
        for (int i = 0; i < number; i++) {
            intConstructor.add(String.valueOf(i));
        }
    }

    @Before
    public void initTest() {
        defaultConstructor = new CustomArrayImpl<>();
        intConstructor = new CustomArrayImpl<>(0);
        intConstructorNotNul = new CustomArrayImpl<>(10);
        new CustomArrayImpl<>(10);
        collection = new ArrayList<>();
        addToCollection();
        collectionConstructor = new CustomArrayImpl<>(collection);
    }

    @Test
    public void intConstructorTest() {
        String[] test = {"1", "2", "3", "4", "5"};
        intConstructorNotNul.addAll(test);
        Assert.assertEquals(test.length, intConstructorNotNul.size());
        for (int i = 0; i < test.length; i++) {
            Assert.assertEquals(test[i], intConstructorNotNul.get(i));
        }

    }

    @Test
    public void sizeDefaultConstructorTest() {
        addToDefault(3);
        Assert.assertEquals(3, defaultConstructor.size());
    }

    @Test
    public void sizeIntConstructorTest() {
        addToInteger(3);
        Assert.assertEquals(3, intConstructor.size());
    }

    @Test
    public void sizeCollectionConstructorTest() {
        Assert.assertEquals(50, collectionConstructor.size());
    }

    @Test
    public void isEmptyTest() {
        Assert.assertTrue(defaultConstructor.isEmpty());
        addToDefault(1);
        Assert.assertFalse(defaultConstructor.isEmpty());
    }

    @Test
    public void addTest() {
        int topPosition = defaultConstructor.size();
        defaultConstructor.add("TestAdd");
        Assert.assertEquals("TestAdd", defaultConstructor.get(topPosition));
    }

    @Test(expected = IllegalArgumentException.class)
    public void addAll_TItemExceptionTest() {
        String[] testStrArray = null;
        defaultConstructor.addAll(testStrArray);
    }

    @Test
    public void addAll_TItemTest() {
        addToDefault(4);
        int boundTest = 29;
        int boundTestArray = boundTest - defaultConstructor.size();
        String[] testStrArray = new String[boundTestArray];
        for (int i = 0; i < boundTestArray; i++) {
            testStrArray[i] = String.valueOf(i + 3);
        }
        defaultConstructor.addAll(testStrArray);
        for (int i = 0; i < boundTest; i++) {
            if (i < 4) {
                Assert.assertEquals(String.valueOf(i), defaultConstructor.get(i));
            } else {
                Assert.assertEquals(String.valueOf((i - 4) + 3), defaultConstructor.get(i));
            }
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void addAll_CollectionExceptionTest() {
        Collection<String> testCollection = null;
        defaultConstructor.addAll(testCollection);
    }

    @Test
    public void addAll_CollectionTest() {
        addToDefault(5);
        Collection<String> testCollection = new ArrayList<>();
        int boundTest = 29;
        int boundTestArray = boundTest - defaultConstructor.size();
        for (int i = 0; i < boundTestArray; i++) {
            testCollection.add(String.valueOf(i + 3));
        }
        defaultConstructor.addAll(testCollection);
        for (int i = 0; i < boundTest; i++) {
            if (i < 5) {
                Assert.assertEquals(String.valueOf(i), defaultConstructor.get(i));
            } else {
                Assert.assertEquals(String.valueOf((i - 5) + 3), defaultConstructor.get(i));
            }
        }
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addAll_IndexItemExceptionIndexTest1() {
        String[] testStrArray = new String[5];
        defaultConstructor.addAll(-1, testStrArray);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addAll_IndexItemExceptionIndexTest2() {
        String[] testStrArray = new String[5];
        defaultConstructor.addAll(defaultConstructor.size(), testStrArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addAll_IndexItemExceptionItemTest() {
        addToDefault(1);
        defaultConstructor.addAll(0, null);
    }

    @Test
    public void addAll_IndexItemTest() {
        addToDefault(5);
        String[] testStrArray = new String[5];
        for (int i = 0; i < 5; i++) {
            testStrArray[i] = String.valueOf(i + 10);
        }
        defaultConstructor.addAll(4, testStrArray);
        for (int i = 0; i < 10; i++) {
            if (i < 4) {
                Assert.assertEquals(String.valueOf(i), defaultConstructor.get(i));
            } else if (i == 9) {
                Assert.assertEquals(String.valueOf(4), defaultConstructor.get(i));
            } else {
                Assert.assertEquals(String.valueOf((i - 4) + 10), defaultConstructor.get(i));
            }
        }
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getExceptionTest1() {
        addToDefault(2);
        Assert.assertEquals(String.valueOf(0), defaultConstructor.get(-1));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getExceptionTest2() {
        addToDefault(2);
        Assert.assertEquals(String.valueOf(0), defaultConstructor.get(2));
    }

    @Test
    public void getTest() {
        addToDefault(3);
        Assert.assertEquals(String.valueOf(0), defaultConstructor.get(0));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setExceptionTest1() {
        intConstructor.set(-1, String.valueOf(10));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setExceptionTest2() {
        intConstructor.set(intConstructor.size(), String.valueOf(10));
    }

    @Test
    public void setTest() {
        addToInteger(5);
        String oldValue = intConstructor.set(2, String.valueOf(10));
        Assert.assertEquals(String.valueOf(2), oldValue);
        Assert.assertEquals(String.valueOf(10), intConstructor.get(2));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeExceptionTest1() {
        addToInteger(5);
        intConstructor.remove(-1);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeExceptionTest2() {
        addToInteger(5);
        intConstructor.remove(intConstructor.size());
    }

    @Test
    public void removeTest() {
        addToInteger(5);
        intConstructor.remove(0);
        for (int i = 0; i < intConstructor.size(); i++) {
            Assert.assertEquals(String.valueOf(i + 1), intConstructor.get(i));
        }
    }

    @Test
    public void removeItemTest() {
        addToDefault(5);
        boolean result = defaultConstructor.remove(String.valueOf(2));
        for (int i = 0; i < defaultConstructor.size(); i++) {
            if (i < 2) {
                Assert.assertEquals(String.valueOf(i), defaultConstructor.get(i));
            } else {
                Assert.assertEquals(String.valueOf(i + 1), defaultConstructor.get(i));
            }
        }
        Assert.assertTrue(result);
        Assert.assertFalse(defaultConstructor.remove(String.valueOf(10)));
    }

    @Test
    public void containsTest() {
        addToDefault(5);
        Assert.assertFalse(defaultConstructor.contains(String.valueOf(10)));
        Assert.assertTrue(defaultConstructor.contains(String.valueOf(3)));
    }

    @Test
    public void indexOfTest() {
        addToDefault(5);
        Assert.assertEquals(-1, defaultConstructor.indexOf(String.valueOf(10)));
        Assert.assertEquals(3, defaultConstructor.indexOf(String.valueOf(3)));
    }

    @Test
    public void ensureCapacityTest() {
        addToDefault(5);
        int capacity = defaultConstructor.getCapacity();
        boolean flagCheck = true;
        if ((capacity - defaultConstructor.size()) < 20) {
            defaultConstructor.ensureCapacity(20);
            if ((defaultConstructor.getCapacity() - defaultConstructor.size()) < 20) {
                flagCheck = false;
            }
        } else {
            defaultConstructor.ensureCapacity(20);
            if (capacity != defaultConstructor.getCapacity()) {
                flagCheck = false;
            }
        }
        Assert.assertTrue(flagCheck);
    }

    @Test
    public void getCapacityTest() {
        addToDefault(5);
        int capacity = defaultConstructor.getCapacity();
        addToDefault(5);
        Assert.assertNotEquals(capacity, defaultConstructor.getCapacity());
    }

    @Test
    public void reverseTest() {
        addToDefault(10);
        defaultConstructor.reverse();
        for (int i = 0; i < 10; i++) {
            Assert.assertEquals(String.valueOf(9 - i), defaultConstructor.get(i));
        }
    }

    @Test
    public void toStringTest() {
        addToDefault(3);
        Assert.assertEquals("[0, 1, 2]", defaultConstructor.toString());
    }

    @Test
    public void toArrayTest() {
        addToDefault(10);
        Object[] testArray = defaultConstructor.toArray();
        for (int i = 0; i < 10; i++) {
            Assert.assertEquals(String.valueOf(i), testArray[i]);
        }
        Collection<Integer> collection = new ArrayList<>();
        CustomArrayImpl<Integer> testConstructor = new CustomArrayImpl<>(collection);
        Object[] testArray1 = testConstructor.toArray();
        Assert.assertEquals(0, testArray1.length);
    }
}