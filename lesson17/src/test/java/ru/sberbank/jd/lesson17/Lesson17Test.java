package ru.sberbank.jd.lesson17;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;

public class Lesson17Test {

    @Test
    public void testGetProfitCase1() {

        Collection<String> result = ProfitCalculator.getProfit("100000", "10", "2");
        Assert.assertTrue(result.contains("Расчет"));
        Assert.assertTrue(result.contains("Итоговая сумма 121000,00 рублей"));
    }

    @Test
    public void testGetProfitCase2() {

        Collection<String> result = ProfitCalculator.getProfit("100 000", "10", "2");
        Assert.assertTrue(result.contains("Результат"));
        Assert.assertTrue(result.contains("Неверный формат данных. <br> Скорректируйте значения"));
    }

    @Test
    public void testGetProfitCase3() {

        Collection<String> result = ProfitCalculator.getProfit("49000", "10", "2");
        Assert.assertTrue(result.contains("Ошибка"));
        Assert.assertTrue(result.contains("Минимальная сумма на момент <br> открытия вклада 50000 рублей"));
    }

    @Test
    public void testPreprocessingParam() {

        String result = PreprocessingParam.getParameter("100 000");
        Assert.assertEquals("100000", result);
        result = PreprocessingParam.getParameter(" 100 00 0 ");
        Assert.assertEquals("100000", result);
    }

}