package ru.sberbank.jd.lesson17;

public class MyInfo {

    private final String firstName = "Владимир";
    private final String secondName = "Сергеевич";
    private final String lastName = "Лёля";
    private final String phone = "+7 (960) 464 13 74";
    private final String hobbies = "бег, прыг, java, &#128211; &ensp; &#128522;";
    private final String bitbucketUrl = "https://bitbucket.org/vladimir_lyolya/";

    public MyInfo() {

    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getHobbies() {
        return hobbies;
    }

    public String getBitbucketUrl() {
        return bitbucketUrl;
    }

    @Override
    public String toString() {
        return "VladimirLyolya{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                ", hobbies='" + hobbies + '\'' +
                ", bitbucketUrl='" + bitbucketUrl + '\'' +
                '}';
    }
}
