package ru.sberbank.jd.lesson17;

public class PreprocessingParam {

    public static String getParameter(String str) {
        return str.trim().replaceAll(" ", "");
    }
}
