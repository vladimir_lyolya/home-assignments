package ru.sberbank.jd.lesson17;

import java.util.ArrayList;
import java.util.Collection;

public class ProfitCalculator {

    private static final int MIN_SUM = 50_000;

    private static double getTotal(double doubleSum, double doublePercentage, double doubleYears) {
        if (doubleYears < 1) return doubleSum;
        doubleSum = doubleSum + (doubleSum / 100) * doublePercentage;
        doubleYears = doubleYears - 1;
        return getTotal(doubleSum, doublePercentage, doubleYears);
    }

    public static Collection<String> getProfit(String sum, String percentage, String years) {

        double doubleSum;
        double doublePercentage;
        double doubleYears;

        Collection<String> result = new ArrayList<>();

        try {
            doubleSum = Double.parseDouble(sum);
            doublePercentage = Double.parseDouble(percentage);
            doubleYears = Double.parseDouble(years);
        } catch (Exception e) {
            result.add("Результат");
            result.add("Неверный формат данных. <br> Скорректируйте значения");
            return result;
        }

        if (doubleSum < MIN_SUM) {
            result.add("Ошибка");
            result.add("Минимальная сумма на момент <br> открытия вклада " +
                    MIN_SUM + " рублей");
            return result;
        }
        double total = getTotal(doubleSum, doublePercentage, doubleYears);
        result.add("Расчет");
        result.add("Итоговая сумма " + String.format("%.2f", total) + " рублей");
        return result;
    }
}
