package ru.sberbank.jd.lesson17;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GreetingServlet extends HttpServlet {

    MyInfo author = new MyInfo();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setAttribute("author", author);
        getServletContext().getRequestDispatcher("/author.jsp").forward(req, resp);
    }
}
