package ru.sberbank.jd.lesson17;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;

@WebServlet("/finance")
public class FinancialServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        getServletContext().getRequestDispatcher("/finance.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String sum = PreprocessingParam.getParameter(req.getParameter("sum"));
        String percentage = PreprocessingParam.getParameter(req.getParameter("percentage"));
        String years = PreprocessingParam.getParameter(req.getParameter("years"));
        Collection<String> result = ProfitCalculator.getProfit(sum, percentage, years);
        req.setAttribute("result", result);
        getServletContext().getRequestDispatcher("/result.jsp").forward(req, resp);
    }
}
