<%@ page isELIgnored="false" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html>

<head>
	<title> Author </title>
	<meta charset = "UTF-8">
	<link rel="stylesheet" type="text/css" href="/css/author.css">
</head>

<body>
    <h1> Инфрмация об авторе </h1>
<table class="commonData">
		<col width="150" align="left" valign="top">
		<col width="300" align="left" valign="top">
		 <tr>
			<td>Фамилия:</td> <td>${author.lastName}</td>
		 </tr>
		 <tr>
			<td> Имя: </td> <td> ${author.firstName}</td>
		</tr>
		<tr>
			<td>Отчество:</td> <td> ${author.secondName}</td>
		</tr>
		<tr>
			<td> Телефон: </td> <td> ${author.phone} </td>
		</tr>
		 <tr>
			<td> Хобби: </td> <td> ${author.hobbies} </td>
			<td>  </td>
		</tr>
		 <tr>
			<td> Bitbucket url: </td>
			<td> <a href=${author.bitbucketUrl}
			    target="__blank" title="Перейти">
			 ${author.bitbucketUrl} </a> </td>
		</tr>
		</table>
		<hr>
		<h4> <a href="/" title="Назад"> Назад </a>  </h4>
</body>

</html>