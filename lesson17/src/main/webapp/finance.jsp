<%@ page isELIgnored="false" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html>

<head>
	<title> Finance </title>
	<meta charset = "UTF-8">
	<link rel="stylesheet" type="text/css" href="/css/finance.css">
</head>
<body>
    <h1> Калькулятор доходности вклада</h1>
    <form method="POST" action="/finance">
        <table class="getData" method="POST" action="/finance">
            <col width="100" align="left" valign="top">
            <col width="350" align="left" valign="top">
             <tr>
                <td>Сумма на момент открытия</td>
                 <td> <input name="sum" value="100 000" size="10"> </td>
             </tr>
             <tr>
                <td> Процентная ставка </td>
                <td> <input name="percentage" value="10" size="10"> </td>
            </tr>
            <tr>
                <td>Количество лет</td>
                <td> <input name="years" value="2" size="10"> </td>
            </tr>
            <tr>
                <td> <input type="submit" class="buttonSubmit" value="Посчитать" </td>
            </tr>
        </table>
    </form>
		<hr>
		<h4> <a href="/" title="Назад"> Назад </a>  </h3>
</body>

</html>