package ru.sberbank.jd.lesson07;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class CustomDigitComparatorTest {
    final int NUMBERELEMENT = 1000;
    List<Integer> testCollection = new ArrayList<>();

    @Before
    public void getCollection() {
        Random generator = new Random(1L);
        for (int i = 0; i < NUMBERELEMENT; i++) {
            testCollection.add(generator.nextInt());
        }
    }

    @Test
    public void test1() {
        Comparator<Integer> comparator = new CustomDigitComparator();
        assertEquals(-1, comparator.compare(4, 5));
    }

    @Test
    public void test2() {
        Comparator<Integer> comparator = new CustomDigitComparator();
        assertEquals(1, comparator.compare(5, 4));
    }

    @Test
    public void test3() {
        Comparator<Integer> comparator = new CustomDigitComparator();
        assertEquals(0, comparator.compare(5, 5));
    }

    @Test
    public void test4() {
        Comparator<Integer> comparator = new CustomDigitComparator();
        assertEquals(0, comparator.compare(4, 4));
    }

    /**
     * После сортировки с применением компоратора CustomDigitComparator
     * в testCollection сначала должны леч все четные значения потом все нечетные
     */
    @Test
    public void customDigitComparatorSort() {
        Comparator<Integer> cmpByEven = new CustomDigitComparator();
        testCollection.sort(cmpByEven);
        boolean flagEven = true;
        for (int i = 0; i < NUMBERELEMENT; i++) {
            if (flagEven) {
                if (testCollection.get(i) % 2 == 0) {
                    assertEquals(0, testCollection.get(i) % 2);
                } else {
                    flagEven = false;
                    assertNotEquals(0, testCollection.get(i) % 2);
                }
            } else {
                assertNotEquals(0, testCollection.get(i) % 2);
            }
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void customDigitComparatorException() {
        Comparator<Integer> cmpByEven = new CustomDigitComparator();
        testCollection.add(null);
        testCollection.sort(cmpByEven);
    }
}