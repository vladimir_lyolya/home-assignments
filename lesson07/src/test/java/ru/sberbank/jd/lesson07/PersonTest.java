package ru.sberbank.jd.lesson07;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PersonTest {

    String[] city = {"Bobruisk", "Krijopol", "Mursilkino", "Chelabinsk"};
    String[] cityCheck = {"Bobruisk", "Chelabinsk", "Krijopol", "Mursilkino"};
    String[] name = {"Nikolay", "Piter", "Dasha", "Marina"};
    String[] nameCheck = {"Dasha", "Marina", "Nikolay", "Piter"};

    List<Person> personOriginal = new ArrayList<>();
    List<Person> personToCompare = new ArrayList<>();
    Person person = new Person("Masha", "Uhta", 27);

    /**
     * Создаем два набора данных с разным регистром полей "name" и "city"
     */
    @Before
    public void getPerson() {
        for (int i = 0; i < city.length; i++) {
            for (String sname : name) {
                personOriginal.add(new Person(sname, city[i], i + 20));
                StringBuilder tmpName = new StringBuilder(personOriginal.get(personOriginal.size() - 1).getName());
                StringBuilder tmpCity = new StringBuilder(personOriginal.get(personOriginal.size() - 1).getCity());
                for (int k = 0; k < tmpName.length(); k++) {
                    if (Character.isUpperCase(tmpName.charAt(k))) {
                        tmpName.setCharAt(k, Character.toLowerCase(tmpName.charAt(k)));
                    } else {
                        tmpName.setCharAt(k, Character.toUpperCase(tmpName.charAt(k)));
                    }
                }
                for (int k = 0; k < tmpCity.length(); k++) {
                    if (Character.isUpperCase(tmpCity.charAt(k))) {
                        tmpCity.setCharAt(k, Character.toLowerCase(tmpCity.charAt(k)));
                    } else {
                        tmpCity.setCharAt(k, Character.toUpperCase(tmpCity.charAt(k)));
                    }
                }
                personToCompare.add(new Person(tmpName.toString(), tmpCity.toString(), i + 20));
            }
        }
    }

    @Test
    public void testEquals() {
        for (int i = 0; i < personOriginal.size(); i++) {
            assertEquals(personOriginal.get(i), personToCompare.get(i));
        }
    }

    @Test
    public void testHashCode() {
        for (int i = 0; i < personOriginal.size(); i++) {
            assertEquals(personOriginal.get(i).hashCode(), personToCompare.get(i).hashCode());
        }
    }

    @Test
    public void testToString() {
        assertEquals("Person{name='Masha', city='Uhta', age='27'}", person.toString());
    }

    @Test
    public void compareToCheck1() {
        Person myPerson1 = new Person("Georg", "oMsk", 5);
        Person myPerson2 = new Person("Oleg", "Primorsk", 5);
        Assert.assertEquals(-1, myPerson1.compareTo(myPerson2));
    }

    @Test
    public void compareToCheck2() {
        Person myPerson1 = new Person("Georg", "Primorsk", 5);
        Person myPerson2 = new Person("Oleg", "oMsk", 5);
        Assert.assertEquals(1, myPerson1.compareTo(myPerson2));
    }

    @Test
    public void compareToCheck3() {
        Person myPerson1 = new Person("Oleg", "Primorsk", 5);
        Person myPerson2 = new Person("Oleg", "Primorsk", 5);
        Assert.assertEquals(0, myPerson1.compareTo(myPerson2));
    }

    @Test
    public void compareToCheck4() {
        Person myPerson1 = new Person("Georg", "Primorsk", 5);
        Person myPerson2 = new Person("Oleg", "Primorsk", 5);
        Assert.assertEquals(-8, myPerson1.compareTo(myPerson2));
    }

    @Test
    public void compareToCheck5() {
        Person myPerson1 = new Person("Oleg", "Primorsk", 5);
        Person myPerson2 = new Person("Georg", "Primorsk", 5);
        Assert.assertEquals(8, myPerson1.compareTo(myPerson2));
    }

    /**
     * После сортировки с применением Person::compareTo
     * обекты должны леч в соответсвии с полями проверочных массивов cityCheck и nameCheck
     */
    @Test
    public void compareTo() {
        personOriginal.sort(Person::compareTo);
        int cityCount = 0;
        int nameCount = 0;
        for (Person value : personOriginal) {
            assertEquals(cityCheck[cityCount], value.getCity());
            assertEquals(nameCheck[nameCount], value.getName());
            nameCount++;
            if (nameCount == nameCheck.length) {
                cityCount++;
                nameCount = 0;
            }
        }
        personToCompare.sort(Person::compareTo);
        for (int i = 0; i < personOriginal.size(); i++) {
            assertEquals(personOriginal.get(i), personToCompare.get(i));
        }
    }

    @Test
    public void getName() {
        assertEquals("Masha", person.getName());
    }

    @Test
    public void getCity() {
        assertEquals("Uhta", person.getCity());
    }

    @Test
    public void getAge() {
        assertEquals(new Integer("27"), person.getAge());
    }

    @Test(expected = RuntimeException.class)
    public void constructorChek1() {
        new Person("", "City", 2);
    }

    @Test(expected = RuntimeException.class)
    public void constructorChek2() {
        new Person(null, "City", 2);
    }

    @Test(expected = RuntimeException.class)
    public void constructorChek3() {
        new Person("Yumi", "", 2);
    }

    @Test(expected = RuntimeException.class)
    public void constructorChek4() {
        new Person("Yumi", null, 2);
    }

    @Test(expected = RuntimeException.class)
    public void constructorChek5() {
        new Person("Yumi", "City", -1);
    }

    @Test(expected = RuntimeException.class)
    public void constructorChek6() {
        new Person("Yumi", "City", 0);
    }
}