package ru.sberbank.jd.lesson07;

import java.util.Objects;

public class Person implements Comparable<Person> {

    private final String name;
    private final String city;
    private final Integer age;

    public Person(String name, String city, Integer age) {
        if (name == null || name.equals("") || city == null || city.equals("") ||
                age <= 0) throw new RuntimeException("name or city or age wrong");
        this.name = name;
        this.city = city;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return name.equalsIgnoreCase(person.name) && city.equalsIgnoreCase(person.city) && age.equals(person.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name.toUpperCase(), city.toUpperCase(), age);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age='" + age + '\'' +
                '}';
    }

    /**
     * Обеспечивает следующий порядок: - Сортировка сначала по полю city, а затем по полю name
     */
    @Override
    public int compareTo(Person other) {
        if (this.city.equalsIgnoreCase(other.city)) {
            return this.name.compareToIgnoreCase(other.name);
        } else {
            return this.city.compareToIgnoreCase(other.city);
        }
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public Integer getAge() {
        return age;
    }

}
