package ru.sberbank.jd.lesson07;

import java.util.Comparator;

public class CustomDigitComparator implements Comparator<Integer> {
    /**
     * Определяет следующий порядок: - Сначала четные числа, затем нечетные - На вход подаются числа, отличные от null
     */
    @Override
    public int compare(Integer lhs, Integer rhs) {
        if (lhs == null || rhs == null) throw new IllegalArgumentException("CustomDigitComparator argument is null");
        lhs = Math.abs(lhs);
        rhs = (Math.abs(rhs));
        if (lhs % 2 == rhs % 2) return 0;
        if (lhs % 2 == 0) return -1;
        else return 1;
    }
}
