package ru.sberbank.jd.lesson14;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Предназначен для тестирования WeatherCache
 * Возвращает актуальный LocalDateTime на момент создания с привязкой к city
 */
public class WeatherInfoTest {

    private final String city;

    public WeatherInfoTest(String city) {
        this.city = city;
    }

    public WeatherInfo get() {
        String shortDescription = city + " shortDescription";
        String description = city + " description";
        double temperature = 199.9;
        double feelsLikeTemperature = 199.9;
        double windSpeed = 199.9;
        double pressure = 199.9;
        LocalDateTime time = LocalDateTime.of(LocalDate.now(), LocalTime.now());
        return new WeatherInfo().builder()
                .setCity(city)
                .setShortDescription(shortDescription)
                .setDescription(description)
                .setTemperature(temperature)
                .setFeelsLikeTemperature(feelsLikeTemperature)
                .setWindSpeed(windSpeed)
                .setPressure(pressure)
                .setExpiryTime(time)
                .build();
    }

}
