package ru.sberbank.jd.lesson14;

/**
 * Предназначен для тестирования WeatherCache
 * хранит WeatherInfo с привязкой к city
 * запрос на обновление WeatherInfo с периодом PERIOD_REQUEST
 */
public class TestTread extends Thread {

    private final long PERIOD_REQUEST = 2_000;
    private final String city;
    private WeatherInfo weatherInfo;
    private final WeatherCache weatherCache;

    public TestTread(String city, WeatherCache weatherCache) {
        this.city = city;
        this.weatherCache = weatherCache;
    }

    public WeatherInfo getWeatherInfo() {
        return weatherInfo;
    }

    @Override
    public void run() {
        while (!this.isInterrupted()) {
            try {
                weatherInfo = weatherCache.getWeatherInfo(city);
                Thread.sleep(PERIOD_REQUEST);
            } catch (InterruptedException ie) {
                break;
            }
        }
    }
}
