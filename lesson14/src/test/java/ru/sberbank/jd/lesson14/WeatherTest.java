package ru.sberbank.jd.lesson14;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class WeatherTest {

    final int CACHE_LIFETIME_SEC = 100;
    final int SLEEP_MONITORING_SEC = 2;
    final int TIME_MONITORING_SEC = 150;
    final int TIME_UPDATE_SEC = CACHE_LIFETIME_SEC - 10;
    final int TIME_REMOVE_FROM_CACHE = CACHE_LIFETIME_SEC + 10;

    WeatherProvider mockWeatherProvider;
    String[] listCityTest = {"Dubrovka", "Udomlya", "Leninskiy", "Asdfg"};

    @Before
    public void createMockObj() {
        mockWeatherProvider = Mockito.mock(WeatherProvider.class);
        Mockito.when(mockWeatherProvider.get(listCityTest[0])).thenReturn(new WeatherInfoTest(listCityTest[0]).get());
        Mockito.when(mockWeatherProvider.get(listCityTest[1])).thenReturn(new WeatherInfoTest(listCityTest[1]).get());
        Mockito.when(mockWeatherProvider.get(listCityTest[2])).thenReturn(new WeatherInfoTest(listCityTest[2]).get());
        Mockito.when(mockWeatherProvider.get(listCityTest[3])).thenReturn(null);
    }

    @Test
    public void WeatherProviderTest() {
        WeatherProvider weatherProvider = new WeatherProvider();
        Assert.assertEquals("Ростов-на-Дону", weatherProvider.get("Rostov-on-Don").getCity());
        Assert.assertNull(weatherProvider.get("Asdfg"));
    }

    @Test
    public void WeatherCacheTestWithRemove() throws InterruptedException {
        WeatherCache weatherCache = new WeatherCache(mockWeatherProvider, CACHE_LIFETIME_SEC);
        TestTread[] treadsCity = new TestTread[listCityTest.length];
        for (int i = 0; i < listCityTest.length; i++) {
            treadsCity[i] = new TestTread(listCityTest[i], weatherCache);
            treadsCity[i].start();
        }
        for (int i = 0; i < TIME_MONITORING_SEC / SLEEP_MONITORING_SEC; i++) {
            Thread.sleep(SLEEP_MONITORING_SEC * 1000);
            //Обновляем LocalDateTime в объектах для новых вызовов из WeatherCache
            if (i == TIME_UPDATE_SEC / SLEEP_MONITORING_SEC) {
                Mockito.when(mockWeatherProvider.get(listCityTest[0])).thenReturn(new WeatherInfoTest(listCityTest[0]).get());
                Mockito.when(mockWeatherProvider.get(listCityTest[1])).thenReturn(new WeatherInfoTest(listCityTest[1]).get());
                Mockito.when(mockWeatherProvider.get(listCityTest[2])).thenReturn(new WeatherInfoTest(listCityTest[2]).get());
            }
            if (i == TIME_REMOVE_FROM_CACHE / SLEEP_MONITORING_SEC) weatherCache.removeWeatherInfo(listCityTest[0]);
            if (i == (TIME_REMOVE_FROM_CACHE / SLEEP_MONITORING_SEC) + 1)
                weatherCache.removeWeatherInfo(listCityTest[1]);
            if (i == (TIME_REMOVE_FROM_CACHE / SLEEP_MONITORING_SEC) + 2)
                weatherCache.removeWeatherInfo(listCityTest[2]);
        }
        for (int i = 0; i < listCityTest.length; i++) {
            treadsCity[i].interrupt();
        }
        for (int i = 0; i < listCityTest.length; i++) {
            treadsCity[i].join();
        }
        //lifeTime cache установлен на 100 сек, за время теста в 150 сек должно быть три вызова, с учетом удаления из кэша
        Mockito.verify(mockWeatherProvider, Mockito.times(3)).get(listCityTest[0]);
        Mockito.verify(mockWeatherProvider, Mockito.times(3)).get(listCityTest[1]);
        Mockito.verify(mockWeatherProvider, Mockito.times(3)).get(listCityTest[2]);
        Mockito.verify(mockWeatherProvider, Mockito.atMost(76)).get(listCityTest[3]);
    }

    @Test
    public void WeatherCacheTest() throws InterruptedException {
        WeatherCache weatherCache = new WeatherCache(mockWeatherProvider, CACHE_LIFETIME_SEC);
        TestTread[] treadsCity = new TestTread[listCityTest.length];
        for (int i = 0; i < listCityTest.length; i++) {
            treadsCity[i] = new TestTread(listCityTest[i], weatherCache);
            treadsCity[i].start();
        }
        for (int i = 0; i < TIME_MONITORING_SEC / SLEEP_MONITORING_SEC; i++) {
            Thread.sleep(SLEEP_MONITORING_SEC * 1000);
            //Обновляем LocalDateTime в объектах для новых вызовов из WeatherCache
            if (i == TIME_UPDATE_SEC / SLEEP_MONITORING_SEC) {
                Mockito.when(mockWeatherProvider.get(listCityTest[0])).thenReturn(new WeatherInfoTest(listCityTest[0]).get());
                Mockito.when(mockWeatherProvider.get(listCityTest[1])).thenReturn(new WeatherInfoTest(listCityTest[1]).get());
                Mockito.when(mockWeatherProvider.get(listCityTest[2])).thenReturn(new WeatherInfoTest(listCityTest[2]).get());
            }
        }
        for (int i = 0; i < listCityTest.length; i++) {
            treadsCity[i].interrupt();
        }
        for (int i = 0; i < listCityTest.length; i++) {
            treadsCity[i].join();
        }
        //lifeTime cache установлен на 100 сек, за время теста в 150 сек должно быть два вызова
        Mockito.verify(mockWeatherProvider, Mockito.times(2)).get(listCityTest[0]);
        Mockito.verify(mockWeatherProvider, Mockito.times(2)).get(listCityTest[1]);
        Mockito.verify(mockWeatherProvider, Mockito.times(2)).get(listCityTest[2]);
        Mockito.verify(mockWeatherProvider, Mockito.atMost(76)).get(listCityTest[3]);
    }
}