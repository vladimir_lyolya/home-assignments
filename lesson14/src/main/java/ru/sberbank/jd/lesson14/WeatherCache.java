package ru.sberbank.jd.lesson14;

import java.time.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Weather cache.
 */
public class WeatherCache {

    private final Map<String, WeatherInfo> cache = new HashMap<>();
    private final WeatherProvider weatherProvider;
    private final ManCache manCache = new ManCache();
    private final ManWeatherProvider manWeatherProvider = new ManWeatherProvider();
    private final int lifeTimeCache;

    /**
     * Потокобезопасный класс работы с cache
     */
    private enum Command {
        PUT,
        GET,
        REMOVE
    }

    private class ManCache {
        private synchronized WeatherInfo runCommand(Command command, String city, WeatherInfo weatherInfo) {
            switch (command) {
                case PUT:
                    cache.put(city, weatherInfo);
                    return null;
                case GET:
                    return cache.get(city);
                case REMOVE:
                    cache.remove(city);
                    return null;
                default:
                    return null;
            }
        }
    }

    /**
     * Потокобезопасный класс работы с WeatherProvider
     */
    private class ManWeatherProvider {
        private synchronized WeatherInfo get(String city) {
            return weatherProvider.get(city);
        }
    }

    /**
     * Constructor.
     *
     * @param weatherProvider - weather provider
     */
    public WeatherCache(WeatherProvider weatherProvider, int lifeTimeCache) {
        this.weatherProvider = weatherProvider;
        this.lifeTimeCache = lifeTimeCache;
    }

    /**
     * Get ACTUAL weather info for current city or null if current city not found.
     * If cache doesn't contain weather info OR contains NOT ACTUAL info then we should download info
     * If you download weather info then you should set expiry time now() plus 5 minutes.
     * If you can't download weather info then remove weather info for current city from cache.
     *
     * @param city - city
     * @return actual weather info
     */
    public WeatherInfo getWeatherInfo(String city) {
        // should be implemented
        WeatherInfo weatherInfo = manCache.runCommand(Command.GET, city, null);
        if (weatherInfo == null) {
            weatherInfo = manWeatherProvider.get(city);
            if (weatherInfo != null) manCache.runCommand(Command.PUT, city, weatherInfo);
        } else {
            LocalDateTime weatherInfoTime = weatherInfo.getExpiryTime();
            OffsetDateTime offsetDateTime = OffsetDateTime.now(ZoneId.systemDefault());
            ZoneOffset zoneOffset = offsetDateTime.getOffset();
            long weatherInfoTimeSec = weatherInfoTime.toEpochSecond(zoneOffset);
            Instant timeStamp = Instant.now();
            long timeStampSec = timeStamp.getEpochSecond();
            if (timeStampSec - weatherInfoTimeSec > lifeTimeCache) {
                weatherInfo = manWeatherProvider.get(city);
                if (weatherInfo != null) manCache.runCommand(Command.PUT, city, weatherInfo);
                return weatherInfo;
            }
        }
        return weatherInfo;
    }

    /**
     * Remove weather info from cache.
     **/
    public void removeWeatherInfo(String city) {
        // should be implemented
        manCache.runCommand(Command.REMOVE, city, null);
    }
}

