package ru.sberbank.jd.lesson14;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Weather provider
 */
public class WeatherProvider {

    private final String urlTemplate = "http://api.openweathermap.org/data/2.5/weather?q=city_name&units=metric&lang=ru&appid=066621fb11770f9beaeb6e7f8f8d27b0";
    private final RestTemplate restTemplate;

    public WeatherProvider() {
        restTemplate = new RestTemplate();
    }

    /**
     * Download ACTUAL weather info from internet.
     * You should call GET http://api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}
     * You should use Spring Rest Template for calling requests
     *
     * @param city - city
     * @return weather info or null
     */
    public WeatherInfo get(String city) {
        String urlWeather = urlTemplate.replaceFirst("city_name", city);
        String response;
        try {
            response = restTemplate.getForObject(urlWeather, String.class);
        } catch (Exception ex) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode jsonNode = mapper.readTree(response);
            String cityFrom = jsonNode.at("/name").asText();
            String shortDescription = jsonNode.findPath("weather").findPath("main").asText();
            String description = jsonNode.findPath("weather").findPath("description").asText();
            double temperature = jsonNode.at("/main/temp").asDouble(99.9);
            double feelsLikeTemperature = jsonNode.at("/main/feels_like").asDouble(99.9);
            double windSpeed = jsonNode.at("/wind/speed").asDouble(99.9);
            double pressure = jsonNode.at("/main/pressure").asDouble(99.9);
            LocalDateTime time = LocalDateTime.of(LocalDate.now(), LocalTime.now());
            return new WeatherInfo().builder()
                    .setCity(cityFrom)
                    .setShortDescription(shortDescription)
                    .setDescription(description)
                    .setTemperature(temperature)
                    .setFeelsLikeTemperature(feelsLikeTemperature)
                    .setWindSpeed(windSpeed)
                    .setPressure(pressure)
                    .setExpiryTime(time)
                    .build();
        } catch (Exception ex) {
            return null;
        }
    }
}
