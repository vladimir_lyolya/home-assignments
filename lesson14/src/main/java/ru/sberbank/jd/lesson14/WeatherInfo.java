package ru.sberbank.jd.lesson14;

import java.time.LocalDateTime;

/**
 * Weather info.
 */
public class WeatherInfo {

    private String city;

    /**
     * Short weather description
     * Like 'sunny', 'clouds', 'raining', etc
     */
    private String shortDescription;

    /**
     * Weather description.
     * Like 'broken clouds', 'heavy raining', etc
     */
    private String description;

    /**
     * Temperature.
     */
    private double temperature;

    /**
     * Temperature that fells like.
     */
    private double feelsLikeTemperature;

    /**
     * Wind speed.
     */
    private double windSpeed;

    /**
     * Pressure.
     */
    private double pressure;

    /**
     * Expiry time of weather info.
     * If current time is above expiry time then current weather info is not actual!
     */
    private LocalDateTime expiryTime;

    public LocalDateTime getExpiryTime() {
        return expiryTime;
    }

    public String getCity() {
        return city;
    }

    public Builder builder() {
        return new Builder();
    }

    public class Builder {

        public WeatherInfo weatherInfo = new WeatherInfo();

        public Builder setCity(String city) {
            weatherInfo.city = city;
            return this;
        }

        public Builder setShortDescription(String shortDescription) {
            weatherInfo.shortDescription = shortDescription;
            return this;
        }

        public Builder setDescription(String description) {
            weatherInfo.description = description;
            return this;
        }

        public Builder setTemperature(double temperature) {
            weatherInfo.temperature = temperature;
            return this;
        }

        public Builder setFeelsLikeTemperature(double feelsLikeTemperature) {
            weatherInfo.feelsLikeTemperature = feelsLikeTemperature;
            return this;
        }

        public Builder setWindSpeed(double windSpeed) {
            weatherInfo.windSpeed = windSpeed;
            return this;
        }

        public Builder setPressure(double pressure) {
            weatherInfo.pressure = pressure;
            return this;
        }

        public Builder setExpiryTime(LocalDateTime expiryTime) {
            weatherInfo.expiryTime = expiryTime;
            return this;
        }

        public WeatherInfo build() {
            return weatherInfo;
        }
    }

    @Override
    public String toString() {
        return "WeatherInfo{" +
                "city='" + city + '\'' +
                ",\nshortDescription='" + shortDescription + '\'' +
                ",\ndescription='" + description + '\'' +
                ",\ntemperature=" + temperature +
                ",\nfeelsLikeTemperature=" + feelsLikeTemperature +
                ",\nwindSpeed=" + windSpeed +
                ",\npressure=" + pressure +
                ",\nexpiryTime=" + expiryTime +
                '}';
    }
}
