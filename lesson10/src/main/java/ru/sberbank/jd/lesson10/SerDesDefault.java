package ru.sberbank.jd.lesson10;

import ru.sberbank.jd.lesson10.output.Registry;

import java.io.*;

public class SerDesDefault {

    public static void writeToDefault(String path, Registry registry) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(path);
        ObjectOutputStream outObj = new ObjectOutputStream(fileOutputStream);
        outObj.writeObject(registry);
        outObj.close();
        fileOutputStream.close();
    }

    public static Registry readFromDefault(String path) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(path);
        ObjectInputStream inObj = new ObjectInputStream(fileInputStream);
        Registry registry = (Registry) inObj.readObject();
        inObj.close();
        fileInputStream.close();
        return registry;
    }
}
