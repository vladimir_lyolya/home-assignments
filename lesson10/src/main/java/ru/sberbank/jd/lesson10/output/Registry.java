package ru.sberbank.jd.lesson10.output;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JacksonXmlRootElement(localName = "ArtistRegistry")
public class Registry implements Serializable {

    @JacksonXmlProperty(isAttribute = true)
    private long countryCount;

    private List<Country> countries = new ArrayList<>();

    public Registry() {
    }

    public Registry(List<Country> countries) {
        this.countries = countries;
    }

    public void setCountryCount(long countryCount) {
        this.countryCount = countryCount;
    }

    public long getCountryCount() {
        return countryCount;
    }

    public void setCountry(List<Country> countries) {
        this.countries = countries;
    }

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Country")

    public List<Country> getCountry() {
        return countries;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Registry registry = (Registry) o;
        return countryCount == registry.countryCount && countries.equals(registry.countries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryCount, countries);
    }

}
