package ru.sberbank.jd.lesson10;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.sberbank.jd.lesson10.output.Registry;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class SerDesXml {

    public static void writeToXml(String path, Registry registry) throws IOException {
        XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        FileOutputStream fileOutputStream = new FileOutputStream(path);
        mapper.writeValue(fileOutputStream, registry);
        fileOutputStream.close();
    }

    public static Registry readFromXml(String path) throws IOException {
        XmlMapper mapper = new XmlMapper();
        FileInputStream fileInputStream = new FileInputStream(path);
        Registry registry = mapper.readValue(fileInputStream, Registry.class);
        fileInputStream.close();
        return registry;
    }
}
