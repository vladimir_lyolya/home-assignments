package ru.sberbank.jd.lesson10;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import ru.sberbank.jd.lesson10.output.Registry;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class SerDesJson {

    public static void writeToJson(String path, Registry registry) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        FileOutputStream fileOutputStream = new FileOutputStream(path);
        mapper.writeValue(fileOutputStream, registry);
        fileOutputStream.close();
    }

    public static Registry readFromJson(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        FileInputStream fileInputStream = new FileInputStream(path);
        Registry registry = mapper.readValue(fileInputStream, Registry.class);
        fileInputStream.close();
        return registry;
    }
}
