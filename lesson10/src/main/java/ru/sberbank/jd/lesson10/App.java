package ru.sberbank.jd.lesson10;

import ru.sberbank.jd.lesson10.input.Catalog;
import ru.sberbank.jd.lesson10.output.Registry;

public class App {
    private static final String INPUT_FOLDER = "lesson10/src/main/resources/input/";
    private static final String OUTPUT_FOLDER = "lesson10/src/main/resources/output/";

    public static void main(String[] args) {
        String pathFromCd = INPUT_FOLDER + "cd_catalog.xml";
        String pathToXml = OUTPUT_FOLDER + "artist_by_country.xml";
        String pathToJson = OUTPUT_FOLDER + "artist_by_country.json";
        String pathToSer = OUTPUT_FOLDER + "artist_by_country.serialized";
        try {
            Catalog catalog = ReadCatalogCd.readCatalog(pathFromCd);
            ChangeToRegistry changeToRegistry = new ChangeToRegistry();
            Registry registry = changeToRegistry.change(catalog);
            SerDesXml.writeToXml(pathToXml, registry);
            SerDesJson.writeToJson(pathToJson, registry);
            SerDesDefault.writeToDefault(pathToSer, registry);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
