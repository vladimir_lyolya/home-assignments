package ru.sberbank.jd.lesson10.input;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.*;

@JacksonXmlRootElement(localName = "CATALOG")
public class Catalog implements Serializable, Iterable<Cd> {
    @JacksonXmlProperty(localName = "CD")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Cd> cds;

    public Catalog() {
        cds = new ArrayList<Cd>();
    }

    public int size() {
        return cds.size();
    }

    public List<Cd> getList() {
        return cds;
    }

    public Cd getCd(int index) {
        if (index >= 0 && index < size()) {
            return cds.get(index);
        } else {
            return null;
        }
    }

    @Override
    public Iterator<Cd> iterator() {
        return cds.iterator();
    }
}
