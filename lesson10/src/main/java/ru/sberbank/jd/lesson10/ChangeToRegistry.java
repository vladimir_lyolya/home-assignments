package ru.sberbank.jd.lesson10;

import ru.sberbank.jd.lesson10.input.Catalog;
import ru.sberbank.jd.lesson10.input.Cd;
import ru.sberbank.jd.lesson10.output.Album;
import ru.sberbank.jd.lesson10.output.Artist;
import ru.sberbank.jd.lesson10.output.Country;
import ru.sberbank.jd.lesson10.output.Registry;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ChangeToRegistry {

    private List<Country> countryList = new ArrayList<>();

    private Function<Cd, Country> mapToCountry = (Cd cd) -> {
        Album album = new Album(cd.getTitle(), cd.getYear());
        Artist artist = new Artist(cd.getArtist(), album);
        Country country = new Country(cd.getCountry(), artist);
        if (countryList.contains(country)) {
            List<Artist> tmpArtist = countryList.get(countryList.indexOf(country)).getArtists();
            if (tmpArtist.contains(artist)) {
                List<Album> tmpAlbum = tmpArtist.get(tmpArtist.indexOf(artist)).getAlbums();
                if (!tmpAlbum.contains(album)) {
                    tmpAlbum.add(album);
                    tmpArtist.get(tmpArtist.indexOf(artist)).setAlbums(tmpAlbum);
                    countryList.get(countryList.indexOf(country)).setArtists(tmpArtist);
                }
            } else {
                tmpArtist.add(artist);
                countryList.get(countryList.indexOf(country)).setArtists(tmpArtist);
            }
        } else {
            countryList.add(country);
        }
        return country;
    };

    /**
     * Преобразует структуру Catalog в структуру Registry
     *
     * @param catalog
     * @return Registry
     */
    public Registry change(Catalog catalog) throws Exception {
        if (catalog == null) throw new Exception("wrong argument catalog");
        long countryCount = catalog.getList().stream()
                .map(mapToCountry)
                .map(m -> m.getName())
                .distinct()
                .count();
        Registry registry = new Registry(countryList);
        registry.setCountryCount(countryCount);
        return registry;
    }
}
