package ru.sberbank.jd.lesson10;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.sberbank.jd.lesson10.input.Catalog;

import java.io.FileInputStream;
import java.io.IOException;

public class ReadCatalogCd {
    /**
     * Заполняет структуру Catalog из xml файла
     *
     * @param path    путь к файлу xml
     * @throws IOException
     */
    public static Catalog readCatalog(String path) throws IOException {
        XmlMapper mapper = new XmlMapper();
        FileInputStream fileInputStream = new FileInputStream(path);
        Catalog catalog = mapper.readValue(fileInputStream, Catalog.class);
        fileInputStream.close();
        return catalog;
    }
}
