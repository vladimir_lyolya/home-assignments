package ru.sberbank.jd.lesson10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.sberbank.jd.lesson10.input.Catalog;
import ru.sberbank.jd.lesson10.output.Registry;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;

public class TestFunction {

    String pathFromCd = "src/main/resources/input/cd_catalog.xml";

    String pathToXml = "src/main/resources/output/artist_by_country.xml";
    String pathToJson = "src/main/resources/output/artist_by_country.json";
    String pathToSer = "src/main/resources/output/artist_by_country.serialized";

    String pathFromXml = "src/test/resources/artist_by_country.xml";
    String pathFromJson = "src/test/resources/artist_by_country.json";
    String pathFromSer = "src/test/resources/artist_by_country.serialized";

    Registry registry;

    @Before
    public void beforeTest() {
        try {
            Catalog catalog = ReadCatalogCd.readCatalog(pathFromCd);
            ChangeToRegistry changeToRegistry = new ChangeToRegistry();
            registry = changeToRegistry.change(catalog);
            SerDesXml.writeToXml(pathToXml, registry);
            SerDesJson.writeToJson(pathToJson, registry);
            SerDesDefault.writeToDefault(pathToSer, registry);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void testXmlSerDes() {
        try {
            Registry reg = SerDesXml.readFromXml(pathFromXml);
            Assert.assertEquals(registry, reg);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void testJsonSerDes() {
        try {
            Registry reg = SerDesJson.readFromJson(pathFromJson);
            Assert.assertEquals(registry, reg);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void testDefaultSerDes() {
        try {
            Registry reg = SerDesDefault.readFromDefault(pathFromSer);
            Assert.assertEquals(registry, reg);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
