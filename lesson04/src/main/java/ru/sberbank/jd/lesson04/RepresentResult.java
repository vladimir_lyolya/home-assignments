package ru.sberbank.jd.lesson04;

public class RepresentResult {

    private long countLine;
    private long countWord;
    private long countByte;
    private long maxLenLine;

    /**
     * Выводит результат работы в заданном формате
     */
    public void printResult(WordCount[] wordCounts, CommandLineHandler handler) {
        for (int i = 0; i < wordCounts.length; i++) {
            countLine += wordCounts[i].getCountLine();
            countWord += wordCounts[i].getCountWord();
            countByte += wordCounts[i].getCountByte();
            if (maxLenLine < wordCounts[i].getMaxLenLine()) maxLenLine = wordCounts[i].getMaxLenLine();
            if (handler.isFlagLines()) System.out.printf("%8d", wordCounts[i].getCountLine());
            if (handler.isFlagWords()) System.out.printf("%8d", wordCounts[i].getCountWord());
            if (handler.isFlagBytes()) System.out.printf("%8d", wordCounts[i].getCountByte());
            if (handler.isFlagCount()) System.out.printf("%8d", wordCounts[i].getCountByte());
            if (handler.isFlagMaxLengthLine()) System.out.printf("%8d", wordCounts[i].getMaxLenLine());
            if (handler.getListOfFile().size() != 0) {
                System.out.print("\t" + handler.getPathFile(i) + "\n");
            } else
                System.out.print("\t" + "" + "\n");
        }
        if (handler.getListOfFile().size() > 1) {
            if (handler.isFlagLines()) System.out.printf("%8d", countLine);
            if (handler.isFlagWords()) System.out.printf("%8d", countWord);
            if (handler.isFlagBytes()) System.out.printf("%8d", countByte);
            if (handler.isFlagCount()) System.out.printf("%8d", countByte);
            if (handler.isFlagMaxLengthLine()) System.out.printf("%8d", maxLenLine);
            System.out.print("\t" + "total" + "\n");
        }
    }
}
