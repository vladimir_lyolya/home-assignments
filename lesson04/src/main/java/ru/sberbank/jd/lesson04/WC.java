package ru.sberbank.jd.lesson04;

public class WC {

    public static void main(String[] args) {

        CommandLineHandler handler = new CommandLineHandler();

        String error = handler.argumentAnalysis(args);
        if (!error.equals("")) {
            System.out.print(error);
            return;
        }
        if (handler.isFlagHelp()) {
            System.out.print(handler.getHelp());
            return;
        }
        if (handler.isFlagVersion()) {
            System.out.print("version : " + handler.getVersion());
            return;
        }

        if (handler.getListOfFile().size() == 0) {
            //Читаем с клавиатуры
            WordCount[] wordCount = new WordCount[1];
            wordCount[0] = new WordCount("UTF-8");
            try {
                wordCount[0].readConsole();
                RepresentResult representResult = new RepresentResult();
                representResult.printResult(wordCount, handler);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            WordCount[] wordCount = new WordCount[handler.getListOfFile().size()];
            for (int i = 0; i < handler.getListOfFile().size(); i++) {
                wordCount[i] = new WordCount("UTF-8");
                try {
                    wordCount[i].readFile(handler.getPathFile(i));
                } catch (Exception err) {
                    System.out.println(err.getMessage());
                }
            }
            RepresentResult representResult = new RepresentResult();
            representResult.printResult(wordCount, handler);
        }
    }
}
