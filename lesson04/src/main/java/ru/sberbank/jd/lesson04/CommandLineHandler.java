package ru.sberbank.jd.lesson04;

import java.util.ArrayList;

public class CommandLineHandler {

    final String version = "0.9";

    private boolean flagBytes;          //print the byte counts
    private boolean flagCount;          //print the character counts
    private boolean flagLines;          //print the newline counts
    private boolean flagWords;          //print the word counts
    private boolean flagMaxLengthLine;  //print the length of the longest line
    private boolean flagVersion;        //print version
    private boolean flagHelp;           //print help
    private ArrayList<String> listOfFile = new ArrayList<>(); //имена файлов полученные из коммандной строки

    public CommandLineHandler() {

    }

    /**
     * Заполняет поля класса в соответствии с аргументами
     *
     * @param args Строка аргументов
     * @return Пустая страка или сообщение об ошибке в строке аргументов
     */
    public String argumentAnalysis(String[] args) {
        String errMessage = "";
        if (args.length == 0) {
            flagLines = true;
            flagWords = true;
            flagBytes = true;
            return errMessage;
        } else {
            for (String tmpStr : args) {
                switch (tmpStr) {
                    case "--help":
                        flagHelp = true;
                        break;
                    case "--version":
                        flagVersion = true;
                        break;
                    case "-c":
                    case "--bytes":
                        flagBytes = true;
                        break;
                    case "-m":
                    case "--chars":
                        flagCount = true;
                        break;
                    case "-l":
                    case "--lines":
                        flagLines = true;
                        break;
                    case "-L":
                    case "--max-line-length":
                        flagMaxLengthLine = true;
                        break;
                    case "-w":
                    case "--words":
                        flagWords = true;
                        break;
                    default: {
                        if (tmpStr.length() > 1 &&
                                (tmpStr.charAt(0) == '-' || (tmpStr.charAt(0) == '-' && tmpStr.charAt(1) == '-'))) {
                            errMessage = "wrong argument. use --help";
                        } else {
                            listOfFile.add(tmpStr);
                        }
                    }
                }
                if (flagVersion) break;
                if (flagHelp) break;
            }
        }
        return errMessage;
    }

    public String getVersion() {
        return version;
    }

    public String getHelp() {
        return "-c, --bytes\n\tprint the byte counts\n" +
                "-m, --chars\n\tprint the character counts\n" +
                "-l, --lines\n\tprint the newline counts\n" +
                "-L, --max-line-length\n\tprint the length of the longest line\n" +
                "-w, --words\n\tprint the word counts\n" +
                "--help\n\tdisplay this help and exit\n" +
                "--version\n\toutput version information and exit\n";
    }

    @Override
    public String toString() {
        return "CommandLineHandler{" +
                "version='" + version + '\'' +
                ", flagBytes=" + flagBytes +
                ", flagCount=" + flagCount +
                ", flagLines=" + flagLines +
                ", flagWords=" + flagWords +
                ", flagMaxLengthLine=" + flagMaxLengthLine +
                ", flagVersion=" + flagVersion +
                ", flagHelp=" + flagHelp +
                ", listOfFile=" + listOfFile +
                '}';
    }

    public boolean isFlagCount() {
        return flagCount;
    }

    public boolean isFlagMaxLengthLine() {
        return flagMaxLengthLine;
    }

    public ArrayList<String> getListOfFile() {
        return listOfFile;
    }

    public String getPathFile(int i) {
        return listOfFile.get(i);
    }

    public boolean isFlagVersion() {
        return flagVersion;
    }

    public boolean isFlagHelp() {
        return flagHelp;
    }

    public boolean isFlagBytes() {
        return flagBytes;
    }

    public boolean isFlagLines() {
        return flagLines;
    }

    public boolean isFlagWords() {
        return flagWords;
    }
}
