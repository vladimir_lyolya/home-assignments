package ru.sberbank.jd.lesson04;

import java.io.*;
import java.util.ArrayList;

/**
 * Использование:
 * вызвать argumentAnalysis
 * в зависимости от флагов полученных при анализе в argumentAnalysis вызвать
 * readConsole() - для анализа потока из консоли или
 * readFile(i) - для анализа файлов, где i - индекс файла из listOfFile
 */
public class WordCount {

    private long countByte;
    private long countWord;
    private long countLine;
    private long maxLenLine;
    private String encodingChar;

    /**
     * WordCount(String encodingChar)
     *
     * @param encodingChar Задает формат символов получаемых из консоли
     */
    public WordCount(String encodingChar) {
        this.encodingChar = encodingChar;
    }

    /**
     * Подсчитывает количество строк, слов в строке, максимальную длинну строки
     *
     * @param stringLine строка для разбора
     */
    public void splitLine(String stringLine) {
        countLine++;
        if (stringLine.length() > maxLenLine) maxLenLine = stringLine.length();
        String[] tmpStr = stringLine.split(" "); //Здесь внести возможные разделители слов
        if (tmpStr[tmpStr.length - 1].equals("\n") && tmpStr[0].equals("")) {
            countWord += tmpStr.length - 2;
        } else if (tmpStr[tmpStr.length - 1].equals("\n") || tmpStr[0].equals("")) {
            countWord += tmpStr.length - 1;
        } else countWord += tmpStr.length;
    }

    /**
     * Принимает на вход строку в виде ArrayList<Byte> и меняет значения счетчиков
     * countByte, countLine, countWord, maxLenLine
     * Кодировка символов задается полем encodingChar (по умолчанию UTF-8)
     *
     * @param lineOfByte строка полученных байт в виде ArrayList<Byte>
     */
    public void processLine(ArrayList<Byte> lineOfByte) throws UnsupportedEncodingException {
        int positionEnd = lineOfByte.indexOf(new Byte("-1"));
        if (positionEnd == -1) positionEnd = lineOfByte.size();
        byte[] kk = new byte[positionEnd];
        for (int i = 0; i < positionEnd; i++) {
            kk[i] = lineOfByte.get(i);
        }
        if (kk.length > 0) {
            countByte += kk.length;
            String ss = new String(kk, encodingChar);
            splitLine(ss);
        }
    }

    /**
     * Читает символы с клавиатуры и ведет статистику аналогично программе wc
     * Отличия: ввод данных должен быть завершен переводом строки.
     * Как получить доступ к данным в буфере без перевода строки, не нашел.
     *
     * @throws Exception IOException
     */
    public void readConsole() throws Exception {

        try (InputStream inputStream = System.in) {
            ArrayList<Byte> lineOfByte = new ArrayList<>();
            int inChar;
            int countByte = 0;
            do {
                inChar = inputStream.read();
                lineOfByte.add((byte) inChar);
                countByte++;
                if (inChar == 10) {
                    processLine(lineOfByte);
                    countByte = 0;
                    lineOfByte.clear();
                }
            } while (inChar != -1);
            if (countByte != 0) processLine(lineOfByte);
        } catch (IOException ex) {
            throw new Exception("Error readConsole()");
        }
    }

    /**
     * Читает символы из файла и ведет статистику аналогично программе wc
     *
     * @param pathToFile путь к фалу для обработки
     * @throws Exception Ошибки FileNotFoundException и IOException
     */
    public void readFile(String pathToFile) throws Exception {
        try (BufferedReader bufferReader = new BufferedReader(
                new FileReader(pathToFile))) {
            countByte = (int) (new File(pathToFile)).length();
            String line;
            while ((line = bufferReader.readLine()) != null) {
                splitLine(line);
            }
        } catch (FileNotFoundException notF) {
            throw new Exception("File \"" + pathToFile + "\" not found");
        } catch (IOException ioErr) {
            throw new Exception("Error I/O, file : " + pathToFile);
        }
    }

    public long getCountByte() {
        return countByte;
    }

    public long getCountWord() {
        return countWord;
    }

    public long getCountLine() {
        return countLine;
    }

    public long getMaxLenLine() {
        return maxLenLine;
    }

}
