package ru.sberbank.jd.lesson04;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

public class WCTest {

    private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;
    private ByteArrayOutputStream testOut;

    private void provideInput(String string) {
        testIn = new ByteArrayInputStream(string.getBytes());
        System.setIn(testIn);
    }

    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    @Test
    public void testConsole() {
        String testString = "11 12 \n13 14\n test word";
        provideInput(testString);
        WC.main(new String[0]);
        String stringToCheck = testOut.toString();
        Assert.assertEquals(("       3       6      23\t\n"), stringToCheck);
    }

    @Test
    public void testConsoleOne() {
        String testString = "11 12 \n13 14\n test word";
        provideInput(testString);
        String[] args = {"-c"};
        WC.main(args);
        String stringToCheck = testOut.toString();
        Assert.assertEquals(("      23\t\n"), stringToCheck);
    }

    @Test
    public void testConsoleTwo() {
        String testString = "11 12 \n13 14\n test word";
        provideInput(testString);
        String[] args = {"-c", "--words"};
        WC.main(args);
        String stringToCheck = testOut.toString();
        Assert.assertEquals(("       6      23\t\n"), stringToCheck);
    }

    @Test
    public void testConsoleThree() {
        String testString = "11 12 \n13 14\n test word";
        provideInput(testString);
        String[] args = {"-c", "-w", "-l"};
        WC.main(args);
        String stringToCheck = testOut.toString();
        Assert.assertEquals(("       3       6      23\t\n"), stringToCheck);
    }

    @Test
    public void testFileLongArg() {
        String testString;
        String[] args = {"--bytes", "--chars", "--words", "--max-line-length", "--lines", "./lesson04/src/test/resources/11.txt"};
        WC.main(args);
        testString = "       2       5      15      15       8\t./lesson04/src/test/resources/11.txt\n";
        Assert.assertEquals(testString, testOut.toString());
    }

    @Test
    public void testFileOne() {
        String testString;
        String[] args = {"-c", "-m", "-w", "-L", "-l", "./lesson04/src/test/resources/11.txt"};
        WC.main(args);
        testString = "       2       5      15      15       8\t./lesson04/src/test/resources/11.txt\n";
        Assert.assertEquals(testString, testOut.toString());
    }

    @Test
    public void testFileTwo() {
        String testString;
        String[] args = {"-c", "-w", "-l", "./lesson04/src/test/resources/11.txt", "./lesson04/src/test/resources/22.txt", "-L"};
        WC.main(args);
        testString = "       2       5      15       8\t./lesson04/src/test/resources/11.txt\n" +
                "       2       8      27      20\t./lesson04/src/test/resources/22.txt\n" +
                "       4      13      42      20\ttotal\n";
        Assert.assertEquals(testString, testOut.toString());
    }

    @Test
    public void testHelp() {
        String testString;
        String[] args = {"--help"};
        WC.main(args);
        testString = "-c, --bytes\n\tprint the byte counts\n" +
                "-m, --chars\n\tprint the character counts\n" +
                "-l, --lines\n\tprint the newline counts\n" +
                "-L, --max-line-length\n\tprint the length of the longest line\n" +
                "-w, --words\n\tprint the word counts\n" +
                "--help\n\tdisplay this help and exit\n" +
                "--version\n\toutput version information and exit\n";
        Assert.assertEquals(testString, testOut.toString());
    }

    @Test
    public void testVersion() {
        String testString;
        String[] args = {"--version"};
        WC.main(args);
        testString = "version : 0.9";
        Assert.assertEquals(testString, testOut.toString());
    }

    @Test
    public void testError() {
        String testString;
        String[] args = {"-k"};
        WC.main(args);
        testString = "wrong argument. use --help";
        Assert.assertEquals(testString, testOut.toString());
    }
}